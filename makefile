#defining the compiler
JC = javac

#defining complier flags
JFLAGS = -Xlint -cp './WordNet/src:../lib/*' -d './WordNet/classes'

#overriding any default targets 
.SUFFIXES: .java .class

#target entry for creating .class files from .java files
#the target entry uses the suffix rule syntax DSTS: rule 
#where 'TS' is the suffix target file and 'DS' is the suffix of the dependency.
.java.class:
	$(JC) $(JFLAGS) $*.java

#CLASSES is a macro consisting of n words, one for each java source file.
CLASSES = \
		  ./WordNet/src/WordNet.java \
		  ./WordNet/src/SAP.java

#default make target entry
default: classes

#replaces .java with .class in CLASSES using a macro $(name:string1=string2)
classes: $(CLASSES:.java=.class)

#RM is a predefined macro in make (RM is rm -f)
clean:
	$(RM) *.class
