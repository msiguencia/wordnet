import java.util.*;

public class WordNet{

    Digraph wordNet;
    Map<String, Set<Integer> > nounTable;

    //constructor that takes the name of two input files
    public WordNet(String synsets, String hypernyms){
        int synsetsProcessed = buildNounTable(synsets);
        wordNet = buildARGraph(hypernyms, synsetsProcessed);
    }

    private int buildNounTable(String synsetFile){
        if (synsetFile == null)
            throw new NullPointerException();
        In in = new In(synsetFile);
        int maxID = 0;
        int synsetsProcessed = 0;
        nounTable = new HashMap<String, Set<Integer> >();

        try{
            while(in.hasNextLine()){
                String line = in.readLine();
                String[] synset = parseSynset(line);
                int synsetID = parseSynsetID(synset[0]);
                String[] nouns = parseNouns(synset[1]);

                if (maxID < synsetID)
                    maxID = synsetID;

                addNouns(nouns, synsetID);
                synsetsProcessed++;
            }
        }
        finally{
            in.close();
        }
        if (maxID != synsetsProcessed-1)
            throw new IllegalArgumentException("Each line must have a"
                    + "unique synset id.");
        return synsetsProcessed;
    }

    private String[] parseSynset(String line){
        if (line == null)
            throw new NullPointerException();
        if (!isDimValid(line))
            throw new IllegalArgumentException(line + " must take the following"
                    + " format: synset_id,noun_list,gloss");
        return line.trim().split(",", 3);
    }

    private boolean isDimValid(String line){
        if (line == null)
            throw new NullPointerException();
        boolean value = false;
        if (line.length() != 0){
            String[] synset = line.trim().split(",", 3);
            if (synset.length == 3)
                value = true;
        }
        return value;
    }

    private int parseSynsetID(String id){
        if (id == null)
            throw new NullPointerException();
        int synsetID;
        try{
            synsetID = Integer.parseInt(id);
        }
        catch (NumberFormatException nfe){
            synsetID = -1;
        }
        if (synsetID < 0)
            throw new IllegalArgumentException(id +" is an invalid synset id.");
        return synsetID;
    }

    private String[] parseNouns(String nouns){
        if (nouns == null)
            throw new NullPointerException();
        if (!validNouns(nouns))
            throw new IllegalArgumentException("There are no nouns to parse.");
        return nouns.trim().split(" ");
    }

    private boolean validNouns(String nouns){
        if (nouns == null)
            throw new NullPointerException();
        String[] list = nouns.trim().split("\\s+");
        boolean value = true;
        if (list.length == 1)
            if (list[0].length() == 0)
                value = false;
        return value;
    }
    private void addNouns(String[] nouns, int synsetID){
        if (nouns == null)
            throw new NullPointerException();
        if (synsetID < 0)
            throw new IllegalArgumentException(synsetID + "is an invalid "
                    + "synset_id, cannot add nouns to the noun table.");
        for (String noun: nouns){
            if (!nounTable.containsKey(noun))
                nounTable.put(noun, new HashSet<Integer>());
            Set<Integer> set = nounTable.get(noun);
            if (!set.contains(synsetID))
                set.add(synsetID);
        }
    }

    private Digraph buildARGraph(String hypernymsFile, int synsets){
        if (hypernymsFile == null)
            throw new NullPointerException();
        if (synsets < 0)
            throw new IllegalArgumentException("The number of synsets cannot be"
                    + " less than 0.");
        In in = new In(hypernymsFile);
        Digraph G = new Digraph(synsets);
        try {
            while(in.hasNextLine()){
                String line = in.readLine();
                int[] hypernyms = parseHypernyms(line);
                if (!addHypernyms(hypernyms, G))
                    throw new IllegalArgumentException("One or all of the "
                            + "following hypernyms are not between 0 and " +
                            + G.V() + " : " + Arrays.toString(hypernyms));
            }
        }
        finally{
            in.close();
        }
        return G;
    }

    private int[] parseHypernyms(String line){
        return null;
    }
    private boolean addHypernyms(int[] hypernyms, Digraph G){return true;}

    //A set of distinct nouns as an iterable structure
    public Iterable<String> nouns(){return null;}

    //is the word a noun?
    public boolean isNoun(){return false;}

    //the distance between nounA and nounB
    public int distance(String nounA, String nounB){return 0;}
    
    //a synset that is the common ancestor of nounA and nounB in a shortest
    //ancestral path
    public String sap(String nounA, String nounB){return null;}

    //for unit testing
    public static void main(String[] args){
        WordNet wdnet = new WordNet(args[0], args[1]);
    }
}
