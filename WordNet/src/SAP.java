import java.util.Iterator;
import java.util.*;

public class SAP{
    Digraph G;

    //the constructor takes in as input ANY directed graph
    public SAP(Digraph G){
        this.G = new Digraph(G);
    }

    //the length of the shortest ancestral path between two vertices v and w; -1
    //if no such path exists
    public int length(int v, int w){
        if (v < 0 || v > G.V()-1)
            throw new IndexOutOfBoundsException(v + "is not a vertex of the "
                    + "graph.");
        if (w < 0 || w > G.V()-1)
            throw new IndexOutOfBoundsException(w + "is not a vertex of the "
                    + "graph.");
        if (v == w)
            return 0;

        BreadthFirstDirectedPaths vBFS = new BreadthFirstDirectedPaths(G, v);
        BreadthFirstDirectedPaths wBFS = new BreadthFirstDirectedPaths(G, w);

        int ancestor = ancestor(vBFS, wBFS);
        int length = -1;

        if (ancestor != -1)
            length = vBFS.distTo(ancestor) + wBFS.distTo(ancestor);

        return length;

    }
    //a common ancestor of two vertices v and w that participates in the
    //shortest ancestral path; -1 if no such path exists
    public int ancestor(int v, int w){
        if (v < 0 || v > G.V()-1)
            throw new IndexOutOfBoundsException(v + "is not a vertex of the "
                    + "graph.");
        if (w < 0 || w > G.V()-1)
            throw new IndexOutOfBoundsException(w + "is not a vertex of the "
                    + "graph.");
        if (v == w)
            return v;

        BreadthFirstDirectedPaths vBFS = new BreadthFirstDirectedPaths(G, v);
        BreadthFirstDirectedPaths wBFS = new BreadthFirstDirectedPaths(G, w);

        return ancestor(vBFS, wBFS);
    }

    private int ancestor(BreadthFirstDirectedPaths v,
            BreadthFirstDirectedPaths w){
        int ancestor = -1;
        int INFINITY = Integer.MAX_VALUE;
        int minDist = INFINITY;

        for (int u = 0; u < G.V(); u++)
            if ( v.hasPathTo(u) && w.hasPathTo(u) ){
                int dvu = v.distTo(u);
                int dwu = w.distTo(u);
                int pathDist = dvu + dwu;

                if (pathDist < minDist){
                    ancestor = u;
                    minDist = pathDist;
                }
            }

        return ancestor;
    }

    //the length of the shortest ancestral path between ANY vertex in v and ANY
    //vertex in w; -1 if no such path exists
    public int length(Iterable<Integer> v, Iterable<Integer> w){
        if(v == null || w == null)
            throw new NullPointerException("Cannot find the length of SAP with "
                    + " no vertices.");
        if ( isOutOfBounds(v) )
            throw new IndexOutOfBoundsException("First Iterable contains an "
                    + "invalid vertex");
        if ( isOutOfBounds(w) )
            throw new IndexOutOfBoundsException("Second Iterable contains an "
                    + "invalid vertex");

        BreadthFirstDirectedPaths vBFS = new BreadthFirstDirectedPaths(G, v);
        BreadthFirstDirectedPaths wBFS = new BreadthFirstDirectedPaths(G, w);

        int ancestor = ancestor(vBFS, wBFS);
        int length = -1;

        if (ancestor != -1)
            length = vBFS.distTo(ancestor) + wBFS.distTo(ancestor);

        return length;
    }
    //a common ancestor that participates in the shortest ancestral path; if the
    //path does not exist, then return -1
    public int ancestor(Iterable<Integer> v, Iterable<Integer> w){
        if(v == null || w == null)
            throw new NullPointerException("Cannot find the length of SAP with "
                    + " no vertices");
        if ( isOutOfBounds(v) )
            throw new IndexOutOfBoundsException("First Iterable contains an "
                    + "invalid vertex");
        if ( isOutOfBounds(w) )
            throw new IndexOutOfBoundsException("Second Iterable contains an "
                    + "invalid vertex");

        BreadthFirstDirectedPaths vBFS = new BreadthFirstDirectedPaths(G, v);
        BreadthFirstDirectedPaths wBFS = new BreadthFirstDirectedPaths(G, w);

        return ancestor(vBFS, wBFS);
    }

    private boolean isOutOfBounds(Iterable<Integer> set){
        if (set == null)
            throw new NullPointerException("Cannot determine whether an "
                     + "Iterable has valid vertices when it is null");

        for (int v : set)
            if (v < 0 || v > G.V()-1)
                return true;

        return false;
    }

    //for unit testing of this class
    public static void main(String[] args){
        Digraph G = new Digraph( new In(args[0]) );
        SAP sap = new SAP(G);

        //Tests ancestor and length using all pairs of vertices from G
        //as arguments.

        StringBuilder s = new StringBuilder();
        String newline = System.getProperty("line.separator");

        for (int v = 0; v < G.V(); v++)
            for (int w = 0; w < G.V(); w++){
                String str = String.format("(%d, %d)", v, w) ;
                s.append(String.format("%8s %2d %2d", str, sap.ancestor(v,w),
                           sap.length(v,w) ));
                s.append(newline);
            }

        System.out.println(s);

        //Tests the Iterable versions of the methods ancestor and length using
        //random vertices.

        s = new StringBuilder();
        int random = StdRandom.uniform( G.V() );
        Set<Integer> set1 = new HashSet<Integer>();

        for (int i = 0; i < random; i++){
            int vertex = StdRandom.uniform( G.V() );
            while ( !set1.add(vertex) )
                vertex = StdRandom.uniform( G.V() );
        }

        Set<Integer> set2 = new HashSet<Integer>();

        for (int i = 0; i < random; i++){
            int vertex = StdRandom.uniform( G.V() );
            while ( !set2.add(vertex) )
                vertex = StdRandom.uniform( G.V() );
        }

        String str = new String( String.format("{%s, %s}",
                    set1.toString(), set2.toString()) );

        s.append( String.format("%s %2d %2d", str, sap.ancestor(set1, set2),
                sap.length(set1, set2)) );
        s.append(newline);

        System.out.println(s);
    };
}
